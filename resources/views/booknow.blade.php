  <div class="modal fade" id="bookmodal" role="dialog" aria-labelledby="bookmodalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="bookmodalLabel">
          <h5>{{$item->name}}</h5>
          <h6>{{$item->starttime}}</h6>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modBody modal-body">
    <form method="POST" action="{{url('bookit')}}" id="formBookNow">
      <div style=" width: 45%;float: left"  class="bookNowInput  styled-input rigth">
        <input type="text" required id="bookNowName" />
        <label for="bookNowName">Name Surname</label>
        <span></span>
      </div>
      <div style=" width: 45%;float: right"  class="bookNowInput  styled-input rigth">
        <input type="text" required id="bookNowPhone" />
        <label for="bookNowPhone">Phone Number</label>
        <span></span>
      </div>
      <div style=" width: 100%"  class="bookNowInput  styled-input rigth">
        <input type="email" required id="bookNowEmail" />
        <label for="bookNowEmail">Email</label>
        <span></span>
      </div>
      <input type="hidden" value="{{$item->id}}" class="tour-id" />
      {{csrf_field()}}
      <button type="submit" id="butt" class="btn btn-default buttonSend">Send</button>
    </form>
      </div>
    </div>
  </div>
</div>