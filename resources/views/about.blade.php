    @include('layouts/header')
        <div class="container about">
            <div class="row">
                <div class="col-12 m-top">
                    <h2 id="about-title" class="title text-center head-style-111" >About Us</h2>
                    <p id="text1" class="text-center mx-auto about-us-text">
                        “Your Armenia” is a unique travel agency in Armenia  founded by experts   with 20  
                         year background in tourism field and excellent reputation within
                         industry professionals.We are providing services to travelers wishing
                         to discover the real and authentic Armenia, experience customs and traditions as
                          well as participate in local activities.
                          Our goal is to educate and evoke more authentic travel experience by extending</p> 
                          
                          
                       
                          <p  id="text2" class="text-center mx-auto about-us-text">   to travelers the chance of getting in touch with the real side of Armenia  and experience
                           things that they would never normally experience travelling independently.
                            Learning about Armenia starts by meeting its people. Travelling with a friendly local
                           guide, you’ll also get insight into the region’s special features and locals-only hangouts.  
                        Carefully designed itineraries and tours are for people looking for different travel experience. 
                        We love our country and would be more than glad to help you to find Your Armenia , your story….
                      </p>
                </div>
            </div>
        </div>
        <div class="container container-info m-bottom">
            <div id="googleMap"></div>
            <div class="map-overlay">
                <p> Yerevan, Armenia </hp>
                <p> Address: {{$address->address}}</p>
                <p> E-mail: info@yourarmenia.org </p>
                <p> Tel: +(374-55)206-106 </p>
                <p> Fax: +(374-10)206-106 </p>
                
            </div>
        </div>
        <input type="hidden" value="{{$address->address}}" id="officeAddr" />
@include('layouts/footer')
<script type="text/javascript">
  $(document).ready(function(){
    var address = document.getElementById('officeAddr').value;
    req_data = {
      key: 'AIzaSyAceUm9Fm9GjicBngFDYPuHF-lNF80LH4o',
      address: address.replace(/ /g, '+'),
    };
    $.get('https://maps.googleapis.com/maps/api/geocode/json', req_data, data => {
      if(data.results.length > 0){
        var pyrmont = data.results[0].geometry.location;
        map = new google.maps.Map(document.getElementById('googleMap'), {
          center: pyrmont,
          zoom: 15
        });

        new google.maps.Marker({
          position: pyrmont,
          map: map,
          title: address
        });
      }
    });
  });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAceUm9Fm9GjicBngFDYPuHF-lNF80LH4o"></script>