@include('layouts/header')
    <div class="container text-center margintop">
        <h2 class="title  m-bottom best-offers-header head-style-1"> Best Offers </h2>
        <div class="best-offers-content row">
            @foreach($best_offers as $tour)
            <div class="col-sm-6">
                <div class="grid">
                    <figure class="effect-sadie w-100">
                        <img src="{{url('images/Tours/', $tour->mainpic)}}" alt="{{$tour->name}}"/>
                        <figcaption>
                            <h2>{{$tour->name}}</h2>
                            <p>{{strlen(strip_tags($tour->description, '<script>')) > 50 ? mb_substr(strip_tags($tour->description, '<script>'), 0, 50, "UTF-8") . '...' : strip_tags($tour->description, '<script>')}}</p>
                            <a href="{{url('details', $tour->id)}}">View more</a>
                        </figcaption>			
                    </figure>
                </div>
            </div>
            @endforeach
        </div>
        <div class="clearfix"></div>
        @if($best_offers->lastPage() > 1)
            <button id="view_more" class="btn more btn-more " data-next-page="2"><a>View More</a></button>
        @endif
        <div class="clearfix"></div>
        </div>
        <div  class="a text-center h1 m-bottom mt-100 best-offers-header">
         <span class="title head-style-11"> Explore Armenia with Us </span>
        </div>
        <div id="jssor_1">
        <div data-u="slides" class="slider-content">
            @foreach($home_images as $image)
                <div data-p="137.50">
                    <img data-u="image" src='{{URL("images/Slider/$image->Image")}}' alt="{{$image->Name}}" />
                    <div class="img-title-content">
                        {{$image->Name}}
                    </div>
                </div>
            @endforeach
        </div>
        <div data-u="navigator" class="jssorb051" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i">
                <svg viewbox="0 0 16000 16000">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051 arrow-left" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051 arrow-right" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
        <div class="text-center h1 m-bottom mt-100">
            <h1 class="tit title best-offers-header head-style-1" > Why Choose Us?</h1>
        </div>
        <div class="container whychooseus h1">
            <div class="row bg">
                <div class="col-4 text-center">
                    <div>
                    <i class="fa fa-group f-s-40"></i>
                        <p>Friendly and Qualified Staff</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div>               
                    <i class="material-icons f-s-40">touch_app</i>
                        <p>Online Booking</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div>                       
                    <i class="fa fa-wechat f-s-40"></i>
                        <p>Guest Feedback</p>
                    </div>
                </div>
            </div>
            <div class="row bg">
                <div class="col-4 text-center">
                    <div>   
                    <i class="fa fa-gears f-s-40"></i>
                        <p>Local Expertise</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div>
                    <i class="fa fa-dollar f-s-40"></i>
                        <p>Our Prices</p>
                    </div>
                </div>
                <div class="col-4 text-center">
                    <div>
                    <i class="material-icons f-s-40">access_time</i>
                        <p>Time and Value</p>
                    </div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-sm-6 contact">
                    <h1 class="title best-offers-header" id="Title">Get in Touch with Us</h1>
                    <form id="sendAnswer">
                        <div id="left-inp"  class="styled-input rigth">
                            <input type="text" class="user-name" required />
                            <label>Name</label>
                            <span></span>
                        </div>
                        <div  id="right-inp" class="styled-input rigth">
                            <input type="email" class="user-email" required />
                            <label>Email</label>
                            <span></span>
                        </div>
                        <div  id="full" class="styled-input rigth">
                            <input type="text" class="email-subject" required />
                            <label>Subject</label>
                            <span></span>
                        </div>
                        <div class="styled-input wide">
                            <textarea class="email-description" required></textarea>
                            <label>Description</label>
                            <span></span>
                        </div>
                        <input type="hidden" class="_token" value="{{ csrf_token() }}">
                        <button  type="submit" class="btn btn-default buttonSend">Send</button>
                    </form>
                </div>    
                <div class="col-sm-6 contact small-font">
                    <h2 class="contacts best-offers-header" id="Title1">Contacts</h2>
                    <p>Your Armenia is registered  company (Registration Code No 03A182545).The company is authorized to provide full tourism services in Armenia.</p>
                    <br>
                    <div>
                        <img src="{{url('img/address.png')}}" class="smicon">
                        <b>Address:</b> {{$address->address}}
                    </div>
                   
                    <div >
                        <img src="{{url('img/mail.png')}}" class="smicon">
                        <b>Email:</b> info@yourarmenia.org
                    </div>
                   
                    <div >
                        <img src="{{url('img/phone.png')}}" class="smicon">
                        <b>Tel:</b> +(374-55)206-106
                    </div>
                   
                    <div >
                        <img  src="{{url('img/fax.png')}}" class="smicon">
                        <b>Fax:</b> +(374-10)206-106
                    </div>
                    <div class="social hi-icon-wrap hi-icon-effect-3 hi-icon-effect-3b">
                        <a class="hi-icon icon"><img src="{{url('img/linkedin.png')}}" class="icon"></a>
                        <a class="hi-icon icon"><img src="{{url('img/twitter.png')}}" class="icon"></a>
                        <a class="hi-icon icon"><img src="{{url('img/fb.png')}}" class="icon"></a>
                        <a class="hi-icon icon"><img src="{{url('img/insta.png')}}" class="icon"></a>
                    </div>
                </div>
            </div>
        </div>
@include('layouts/footer')