@include('layouts/header')
    <ul class="category-item-list">  
    @foreach($categories as $item)
        <li class="category-item">
            <a href="{{URL::to($type, $item->id)}}" class="category-item-link @if($item->id == $catId) selected @endif">{{ $item->label }}</a>
            <span class="before">|</span>
        </li>
    @endforeach
    </ul>
    <div class="cont container text-center">
        <div class="row mt-100">
            @foreach($tours as $item)
                <div class="grid col-sm-4">
                    <figure class="{{ $type == 'events' ? 'effect-goliath' :  'effect-ruby'}}">
                        <img src="{{url('images/Tours/', $item->mainpic)}}" alt="{{$item->name}}"/>
                        <figcaption>
                            <h2>{{$item->name}}</h2>
                            <p>{{strlen(strip_tags($item->description, '<script>')) > 20 ? mb_substr(strip_tags($item->description, '<script>'), 0, 20, "UTF-8") . '...' : strip_tags($item->description, '<script>')}}
                                @if($type == 'events')
                                    {{date('Y M d', strtotime($item->starttime))}}
                                @endif
                            </p>
                            <a href="{{url('details', $item->id)}}">View more</a>
                        </figcaption>
                    </figure>
                </div>
            @endforeach
            </div>
            <div class="clearfix"></div>
            <div class="container-fluid text-center">
                <?php echo $tours->links(); ?>
            </div>
        </div>
    </div>
@include('layouts/footer')