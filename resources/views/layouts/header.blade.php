<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <title>{{isset($pageTitle) ? $pageTitle : 'Your Armenia'}}</title>
    <meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="your armenia, armenia, home, about us, tours, packages, short breaks, events, tour in armenia">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(isset($og))
      @foreach($og as $ogKey => $ogItem)
        <meta property="og:{{$ogKey}}" content="{{$ogItem}}">
      @endforeach
    @endif
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/bootstrap.css')}}">
    @if(stripos(Request::route()->uri(), 'home') !== false || Request::route()->uri() === '/' || stripos(Request::route()->uri(), 'details') !== false)
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @endif
    @if(stripos(Request::route()->uri(), 'details') !== false)
      <link rel="stylesheet" href="{{url('css/swiper.min.css')}}">
      <link rel="stylesheet" href="{{url('slider/sliderstyle.css')}}">
    @endif
    @if(stripos(Request::route()->uri(), 'home') !== false || Request::route()->uri() === '/')
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="{{url('css/home.css')}}">
    @endif
    <link rel="stylesheet" href="{{url('css/style.css')}}">
</head>
<body>
  <nav id="navB" class="navbar navbar-expand-lg navbar-light fixed-top">
    <a class="navbar-brand" href="{{url('/')}}"><img class="logo" src="{{url('/img/logo.png')}}"></a>
    <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav float-left">
        <li class="nav-item @if(Request::route()->uri() == '/' || Request::route()->uri() == 'home') active @endif">
          <a class="nav-link" href="{{URL::to('')}}">Home</a>
        </li>
        <li class="nav-item @if(Request::route()->uri() == 'about') active @endif">
          <a class="nav-link" href="{{URL::to('about')}}">About us</a>
        </li>
        <li class="nav-item @if(stripos(Request::route()->uri(), 'tours') !== false || (isset($tourType) && strtolower($tourType) == 'tour')) active @endif">
          <a class="nav-link" href="{{URL::to('tours')}}">Tours</a>
        </li>
        <li class="nav-item @if(stripos(Request::route()->uri(), 'packages') !== false || (isset($tourType) && strtolower($tourType) == 'package')) active @endif">
          <a class="nav-link" href="{{URL::to('packages')}}">Packages</a>
        </li>
       <li class="nav-item  @if(stripos(Request::route()->uri(), 'breaks') !== false || (isset($tourType) && strtolower($tourType) == 'break')) active @endif">
          <a class="nav-link" href="{{URL::to('breaks')}}">Short breaks</a>
        </li>
        <li class="nav-item  @if(stripos(Request::route()->uri(), 'events') !== false || (isset($tourType) && strtolower($tourType) == 'event')) active @endif">
          <a class="nav-link" href="{{URL::to('events')}}">Events</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="headerImg ">
    <img  class="d-block w-100" src="{{url('img/aragats.jpg')}}" alt="Aragats Mountain">
    <h1  class="headerTxt text-center float-right your-local">
      <span class="green" >...your local</span> <span class="f">friend</span>
    </h1>
  </div>