        <footer  class="container-fluid row">
            <div class="footer-social col-sm-12 col-md-4 hi-icon-wrap hi-icon-effect-3 hi-icon-effect-3b">
                <a class="hi-icon icon"><img src="{{url('img/linkedin.png')}}" class="icon"></a>
                <a class="hi-icon icon"><img src="{{url('img/twitter.png')}}" class="icon"></a>
                <a class="hi-icon icon"><img src="{{url('img/fb.png')}}" class="icon"></a>
                <a class="hi-icon icon"><img src="{{url('img/insta.png')}}" class="icon"></a>
            </div>
            <div class="col-sm-12 col-md-8 row">
                <div class="footer-contact col-sm-9 col-md-9">
                    <div class="footer-data-content">
                        <div class="footerContact">
                            <img src="{{url('img/address-white.png')}}" class="smicon">
                            <b>&nbsp;Address:</b> {{$address->address}}
                        </div>
                        <div class="footerContact">
                            <img src="{{url('img/mail-white.png')}}" class="smicon">
                            <b>&nbsp;Email:</b> info@yourarmenia.org
                        </div>
                        <div class="footerContact">
                            <img id="ph" src="{{url('img/phone-white.png')}}" class="smicon-phone smicon">
                            <b>Tel:</b> +(374-55)206-106
                        </div>
                        <div class="footerContact">
                            <img src="{{url('img/fax-white.png')}}" class="smicon">
                            <b>&nbsp;Fax:</b> +(374-10)206-106
                        </div>
                    </div>
                </div>
                <div class=" col-sm-3 col-md-3">
                    <p class="developed-by">Developed by:
                        <a class="site" href="http://www.arssystems.am" target="_blank">Ars Systems</a>
                    </p>
                    <p> &copy; Copyright 2018</p>
                </div>
            </div>
        </footer>
    <script src="{{url('js/jquery.js')}}" ></script>
    <script src="{{url('js/bootstrap.js')}}" ></script>
    <script src="{{url('js/header.js')}}"></script>
    @if(stripos(Request::route()->uri(), 'home') !== false || Request::route()->uri() === '/' || stripos(Request::route()->uri(), 'details') !== false)
        <script src="{{url('js/swiper.min.js')}}"></script>
    @endif
    @if(stripos(Request::route()->uri(), 'home') !== false || Request::route()->uri() === '/')
        <script src="{{url('js/jssor.slider-27.1.0.min.js')}}"></script>
        <script src="{{url('js/home.js')}}"></script> 
    @endif
    @if(stripos(Request::route()->uri(), 'details') !== false)
        <script src="{{url('js/details.js')}}"></script>
    @endif
    </body>
</html>