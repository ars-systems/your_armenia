<div class="modal fade" id="bookmodal" tabindex="-1" role="dialog" aria-labelledby="bookmodalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="bookmodalLabel">
          <h5 id="tour-name"></h5>
          <h6 id="tour-starttime"></h6>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center m-bottom">
          <small>The marked fields are required</small>
        </div>
        <form action="/bookit" method="post" >
          <div class="form-group">
            <label for="name_surname">Name Surname <span class="red">*</span></label>
            <input type="text" name="name" class="form-control" id="name_surname">
          </div>
          <div class="form-group">
            <label for="email">Email <span class="red">*</span></label>
            <input type="email" name="email" class="form-control" id="email" >
          </div>
          <div class="form-group">
            <label for="phone">Phone number <span class="red">*</span></label>
            <input type="text" name="phone" class="form-control" id="phone" >
          </div>
          <div class="form-group">
            <label for="description">Description </label>
            <input type="text" name="description" class="form-control" id="description" >
          </div>
          <input type="hidden" name="tour_id" id="tour-id" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="submit" class="btn send float-right" value="Send">
        </form>
      </div>
    </div>
  </div>
</div>
