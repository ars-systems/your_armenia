@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Settings</div>
                    <div class="panel-body">
                        <form method="POST" action="{{url('admin/settings/address')}}" accept-charset="UTF-8" class="form-horizontal">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$data->id}}" />
                            <div class="form-group">
                                <label for="address" class="col-md-4 control-label">Address: </label>
                                <div class="col-md-6">
                                    <input type="text" name="address" value="{{$data->address}}" required class="form-control" id="address" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input type="submit" value="Save" class="btn btn-primary" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection