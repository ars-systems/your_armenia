<div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $homeimage->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('Image') ? ' has-error' : ''}}" >
    <label for="Image" class="col-md-4 control-label">Main Picture: </label>
    <div class="col-md-6 mainpic-content">
        <input type="file" class="gallery-photo-add mainpic-img" name="Image">
        {!! $errors->first('Image', '<p class="help-block">:message</p>') !!}
        @if(isset($tour->Image))
            <div class="content-pic">
                <input type="hidden" name="mainpic_exists" value="{{$tour->Image}}" />
                <img src="{{URL('images/Tours/', $tour->Image)}}">
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
