<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('place') ? ' has-error' : ''}}">
    {!! Form::label('place', 'Place: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('place', null, ['class' => 'form-control']) !!}
        {!! $errors->first('place', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('types') ? ' has-error' : ''}}">
    <label for="types" class="col-md-4 control-label">Types: </label>
    <div class="col-md-6">
        <select name="type_id" class="form-control">
            @foreach($types as $value)
                @if(isset($tour->type_id) and $value->id == $tour->type_id)
                    <option value="{{$value->id}}" selected >{{$value->name}}</option>
                @else
                    <option value="{{$value->id}}">{{$value->name}}</option>
                @endif
            @endforeach    
        </select>
        {!! $errors->first('types', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('cat_tour') ? ' has-error' : ''}}"> 
    {!! Form::label('cat_tour', 'Categories: ', ['class' => 'col-md-4 control-label']) !!} 
    <div class="col-md-6"> 
        {!! Form::select('categories', $categories, isset($cat_tour) ? $cat_tour : [], ['class' => 'form-control cat-select']) !!} 
    </div> 
</div>
<div class="form-group{{ $errors->has('days') ? ' has-error' : ''}}">
    {!! Form::label('days', 'Days: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('days', null, ['class' => 'form-control']) !!}
        {!! $errors->first('days', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('nights') ? ' has-error' : ''}}">
    {!! Form::label('nights', 'Nights: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('nights', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nights', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('starttime') ? ' has-error' : ''}}">
    {!! Form::label('starttime', 'Starttime: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('starttime', null, ['class' => 'form-control']) !!}
        {!! $errors->first('starttime', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('endtime') ? ' has-error' : ''}}">
    {!! Form::label('endtime', 'Endtime: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('endtime', null, ['class' => 'form-control']) !!}
        {!! $errors->first('endtime', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('is_best', 'Best Offer: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="checkbox" name="is_best" @if(isset($tour->is_best) and $tour->is_best == 1) checked @endif />
    </div>
</div>
<p>*Fill these fields in if you are adding a SHORT BREAK description. </p>
<div id="daysdiv">
    @if(isset($tour->days_descriptions) and count(json_decode($tour->days_descriptions)) > 0)
        @foreach(json_decode($tour->days_descriptions) as $key => $desc)
            <div class="form-group day-desc">
                {!! Form::label('day[]', 'Day'.($key+1).': ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('day[]', $desc, ['class' => 'form-control']) !!}
                    @if($key > 0)
                        <i class="fa fa-times-circle" style="cursor:pointer;"></i>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <div class="form-group">
            {!! Form::label('day[]', 'Day 1: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::textarea('day[]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endif
</div>
<button type="button" class="btn btn-default btn-add-dey-desc" id="add">Add day</button>
<div class="form-group{{ $errors->has('mainpic') ? ' has-error' : ''}}" >
    <label for="mainpic" class="col-md-4 control-label">Main Picture: </label>
    <div class="col-md-6 mainpic-content">
        <input type="file" class="gallery-photo-add mainpic-img" name="mainpic">
        {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
        @if(isset($tour->mainpic))
            <div class="content-pic">
                <input type="hidden" name="mainpic_exists" value="{{$tour->mainpic}}" />
                <img src="{{URL('images/Tours/', $tour->mainpic)}}">
                <button class="btn btn-danger remove-pic">Delete</button>
            </div>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('pics') ? ' has-error' : ''}}">
    <label for="pics" class="col-md-4 control-label">Other pictures: </label>
    <div class="col-md-6">
        <input type="file" multiple class="gallery-photo-add gallery-photo-pics" name="pics[]">
        {!! $errors->first('pics', '<p class="help-block">:message</p>') !!}
        <div class="gallery-pics"></div>
        <div class="removed-gallery-pics"></div>
        @if(isset($tour->pics))
            @foreach(json_decode($tour->pics) as $pic)
                <div class="content-pic">
                    <input type="hidden" class="gallery-exists-pic" name="pics_exists[]" value="{{$pic}}" />
                    <img src="{{URL('images/Tours/', $pic)}}">
                    <button class="btn btn-danger remove-pic">Delete</button>
                </div>
            @endforeach
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(document).off('click', '.btn-add-dey-desc').on('click', '.btn-add-dey-desc', function(event) {
            let n = $('#daysdiv').children().length + 1;
            let day = '<div class="form-group day-desc"><label for="day'+n+'" class="col-md-4 control-label">Day'+n+': </label><div class="col-md-6"><textarea name="day[]" cols="50" rows="10" id="day'+n+'" class="form-control"></textarea><i class="fa fa-times-circle" style="cursor:pointer;"></i></div>';
            $('#daysdiv').append(day);
        });
        $(document).off('click', '.fa-times-circle').on('click', '.fa-times-circle', function(){
            $(this).closest('.day-desc').remove();
            $('.day-desc').each(function(k,v){
                if(k>0){
                    var index = k+=1;
                    $(v).find('textarea').attr('id', 'day'+index);
                    $(v).find('.control-label').attr('for', 'day'+index).text('Day'+index+':');
                }
            });
        });
        $(document).off('click', '.remove-pic').on('click', '.remove-pic', function(){
            var pic_content = $(this).closest('.content-pic');

            if(pic_content.find('.gallery-exists-pic').length > 0){
                var removed_pic_path = pic_content.find('.gallery-exists-pic').val();
                $('.removed-gallery-pics').append('<input type="hidden" name="removed_pic[]" value="'+removed_pic_path+'">');
            }

            pic_content.remove();
        });
        $(document).off('change', '.mainpic-img').on('change', '.mainpic-img', function(){
            $('.mainpic-content').find('.content-pic').remove();
            if(this.files.length > 0){
                var reader = new FileReader();
                reader.onload = function(event) {
                    $('.mainpic-content').append(`<div class="content-pic">
                        <img src="`+event.target.result+`">
                    </div>`);
                }
                reader.readAsDataURL(this.files[0])
            }
        });
        $(document).off('change', '.gallery-photo-pics').on('change', '.gallery-photo-pics', function(){
            $('.gallery-pic').remove();
            if(this.files.length > 0){
                for(var i = 0; i< this.files.length; i++){    
                    var reader = new FileReader();
                    reader.onload = function(event) {
                      $('.gallery-pics').append(`<div class="content-pic gallery-pic">
                            <img src="`+event.target.result+`">
                        </div>`);
                    }
                    reader.readAsDataURL(this.files[i]);
                }
            }
        });
    });
</script>