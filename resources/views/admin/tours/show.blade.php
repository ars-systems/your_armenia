@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Tour</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/tours') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/tours/' . $tour->id . '/edit') }}" title="Edit Role"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/tours', $tour->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Tour',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID.</th> <th>Name</th><th>Type</th></th><th>Place</th><th>Description</th><th>Start time</th><th>End time</th><th>Days</th><th>Nights</th><th>Main image</th><th>Images</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $tour->id }}</td> <td> {{ $tour->name }} </td><td> {{ $tour->type_id }} </td> <td>{{ $tour->place}}</td> <td>{{ $tour->description}}</td> <td>{{ $tour->starttime}}</td> <td>{{ $tour->endtime}}</td> <td>{{ $tour->days}}</td> <td>{{ $tour->nights}}</td><td>{{ $tour->mainpic}}</td><td>{{ $tour->pics}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
