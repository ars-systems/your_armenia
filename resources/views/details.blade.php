@include('layouts/header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-sm-12 content-box details-content">
                <div>
                    <div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <img class="details-main-pic" src="{{url('images/Tours',$item->mainpic)}}"/>
                                @php $pics = json_decode($item->pics); @endphp
                                @if(count($pics) > 1)
                                    <div id="swip" class="swiper-container gallery-thumbs">
                                        <div class="swiper-wrapper">
                                            @foreach($pics as $pic)
                                                <div class="sw swiper-slide show-full-screen" data-toggle="modal" data-target="#sliderModal" data-img="{{url('images/Tours',$pic)}}" style="background-image: url({{url('images/Tours',$pic)}})"></div>
                                            @endforeach
                                        </div>
                                        <div class="swiper-button-next swiper-button-black"></div>
                                        <div class="swiper-button-prev swiper-button-black"></div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-12 details">
                                <h3 class="tour-title">{{$item->name}}</h3>
                                <h6>{{ date('Y F d', strtotime($item->starttime))}} @if(strtolower($tourType) == 'break') {{ ' - ' . date('Y F d', strtotime($item->endtime))}} @endif</h6>
                                <h6>Category: {{$item->tour_category}}</h6>
                                <div class="m-top">
                                    <button class="book btn" data-toggle="modal" data-target="#bookmodal">Book now</button>
                                </div>
                                <p class="m-top">{{ $item->description }}</p>
                                @if($item->days_descriptions && strtolower($tourType) == 'break')
                                    @foreach(json_decode($item->days_descriptions) as $key => $value)
                                        @if(strlen($value) > 0)
                                            <div class='col-12 m-top'>
                                                <h3>Day {{$key + 1}}</h3>
                                                <p>{{$value}}</p>
                                            </div>
                                        @endif
                                    @endforeach 
                                @endif
                            </div>
                        </div>
                        <b>Share on</b>
                        <div class="details-share-btns social hi-icon-wrap hi-icon-effect-3 hi-icon-effect-3b">
                            <a class="hi-icon icon" href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"><img src="{{url('img/linkedin.png')}}" class="icon"></a>
                            <a class="hi-icon icon" href="https://twitter.com/intent/tweet?text={{url()->current()}}"><img src="{{url('img/twitter.png')}}" class="icon"></a>
                            <a class="hi-icon icon" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}"><img src="{{url('img/fb.png')}}" class="icon"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sliderModal" class="modal slider-modal">
        <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
        <div class="pos-abs">
            @if(count($pics) > 1)
                <i class="l slider-btn fa fa-angle-left"></i>
                <i class="r slider-btn fa fa-angle-right"></i>
            @endif
            <img class="modal-content img-full-screen">
        </div>
        <div class="caption"></div>
    </div>
    @include('booknow')
@include('layouts/footer')