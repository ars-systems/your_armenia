<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Tour, HomeImage};
use App\Settings;

class Home extends Controller
{
    public function home()
    {
        $best_offers = Tour::where('is_best', 1)->paginate(4, ['*'], 'page', 1);
        $home_images = HomeImage::get();
        $address     = Settings::getAddress();

        return \View::make('home', compact('best_offers','home_images', 'address'));
    }

    public function bestOffers()
    {
        $data = Tour::where('is_best', 1)->paginate(4, ['id', 'mainpic', 'starttime','name','description']);

        foreach ($data as $tour) {
            $tour->starttime = date('d M Y', strtotime($tour->starttime));
        }

        return $data;
    }

    private function sendmail($name, $email, $subject, $description)
    {
        $to        = env('MAIL_TO', '');
        $message   = "Name: $name ". "Email: $email " . "Message: $description";
        $emailBody = "Here is the user data: {$message}";

        if (strlen($to) > 0) {
            \Mail::send('emails.booking', compact('emailBody'), function($message) use ($to, $subject) {
                $message->to($to)->subject($subject);
            });
        }
    }

    public function email(Request $request)
    {
        $this->validate($request, [ 
            'name'        => 'required',
            'email'       => 'required|email',
            'subject'     => 'required',
            'description' => 'required',
        ]);

        $postData = $request->all();

        try {
            $this->sendmail($postData['name'],$postData['email'],$postData['subject'],$postData['description']);
        } catch (\Exception $e) {
            return \Response::json(['email' => ['Wrong email.']], 422);
        }
    }
}
