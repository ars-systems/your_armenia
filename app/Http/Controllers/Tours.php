<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Tour, Category, Settings}; 

class Tours extends Controller
{
    public function home($catId = null)
    {
        $types = [
            'tours'    => 'tour',
            'packages' => 'package',
            'breaks'   => 'break',
            'events'   => 'event'
        ];

        $titles = [
            'tours'    => 'Tours',
            'packages' => 'Packages',
            'breaks'   => 'Short Break',
            'events'   => 'Events'
        ];

        $type       =  explode('/', \Route::getCurrentRoute()->uri())[0];
        $categories = Category::getByTour($types[$type]);
        $tours      = Tour::getTours($types[$type], $catId);
        $pageTitle  = $titles[$type];
        $address    = Settings::getAddress();

        return \View::make('tours', compact('tours', 'categories', 'type', 'catId', 'pageTitle', 'address'));
    }
}
