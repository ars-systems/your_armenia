<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\{Tour, Type, Category};
use App\Enum\ImgPath;
use Illuminate\Http\Request;

class ToursController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $tours = Tour::where('name', 'LIKE', "%$keyword%")->paginate($perPage);
        } else {
            $tours = Tour::paginate($perPage);
        }

        return view('admin.tours.index', compact('tours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $categories = Category::get()->pluck('label', 'id'); 
        $types      = Type::get();

        return view('admin.tours.create', compact('types', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'starttime'   => 'required',
            'endtime'     => 'required',
            'type_id'     => 'required',
            'mainpic'     => 'required',
            'pics'        => 'required',
        ]);

        $postData = $request->all();

        $postData['mainpic'] = $this->fileUpload($request);
        $postData['pics']    = $this->filesUpload($request);
        $descs               = [];

        if (isset($postData['is_best'])) {
            $postData['is_best'] = $postData['is_best'] == 'on' ? 1 : 0;
        } else {
            $postData['is_best'] = 0;
        }

        foreach ($postData['day'] as $desc) {
            array_push($descs, $desc);
        }

        $postData['days_descriptions'] = json_encode($descs);
              
        $tour = Tour::create($postData);
        
        $tour->assignCategory($request->categories, $tour->id); 

        return redirect('admin/tours')->with('flash_message', 'Tour added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $tour = Tour::findOrFail($id);

        return view('admin.tours.show', compact('tour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $tour       = Tour::findOrFail($id);
        $categories = Category::get()->pluck('label', 'id');
        $types      = Type::get();
        $cat_tour   = \DB::table('category_tour')->where('tour_id', $id)->get()->pluck('category_id');

        return view('admin.tours.edit', compact('tour', 'categories', 'types', 'cat_tour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
                        [
                            'name'        => 'required',
                            'description' => 'required',
                            'starttime'   => 'required',
                            'endtime'     => 'required',
                            'type_id'     => 'required',
                        ]);

        $postData = $request->all();

        $descs             = [];

        foreach ($postData['day'] as $desc) {
            array_push($descs, $desc);
        }

        $postData['days_descriptions'] = json_encode($descs);
        
        if (isset($postData['mainpic']) || !isset($postData['mainpic_exists'])) {
            $postData['mainpic'] = $this->fileUpload($request);
        } else {
            unset($postData['mainpic']);
        }

        if (isset($postData['pics']) && !is_null($postData['pics'][0]) || !isset($postData['pics_exists'])) {
            $postData['pics'] = $this->filesUpload($request);
        } else {
            unset($postData['pics']);
        }

        if (isset($postData['removed_pic'])) {
            foreach ($postData['removed_pic'] as $img) {
                unlink(public_path(ImgPath::TOURS . $img));
            }
        }

        if (isset($postData['pics_exists'])) {
            foreach ($postData['pics_exists'] as $imgName) {
                if(isset($postData['pics']) && !is_array($postData['pics'])){
                    $postData['pics'] = json_decode($postData['pics']);
                }
                $postData['pics'][] = $imgName;
            }

            $postData['pics'] = json_encode($postData['pics']);
        }

        if (isset($postData['is_best'])) {
            $postData['is_best'] = $postData['is_best'] == 'on' ? 1 : 0;
        } else {
            $postData['is_best'] = 0;
        }


        $tour = Tour::findOrFail($id);
        $tour->update($postData);
        if ($request->categories) {
            \DB::table('category_tour')->where('tour_id', $id)->delete();
            $tour->assignCategory($request->categories, $tour->id);  
        }

        return redirect('admin/tours')->with('flash_message', 'Tour updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $item = Tour::find($id);

        $mainPath = public_path('images/Tours/' . $item->mainpic);
        if (file_exists($mainPath)) {
            unlink($mainPath);
        }

        foreach (json_decode($item->pics) as $img) {
            $imgPath = public_path('images/Tours/' . $img);
            if (file_exists($imgPath)) {
                unlink($imgPath);
            }
        }

        Tour::destroy($id);

        return redirect('admin/tours')->with('flash_message', 'Tour deleted!');
    }

    public function fileUpload(Request $request)
    {
        $this->validate($request, [
            'mainpic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        
        $mainpic         = $request->file('mainpic');
        $imagename       = uniqid(time()) . '.' . $mainpic->getClientOriginalExtension();
        $destinationPath = public_path(ImgPath::TOURS);
        
        $mainpic->move($destinationPath, $imagename);

        return $imagename;


    }
    public function filesUpload(Request $request){
        $this->validate($request, ['pics' => 'required']);
        $this->validate($request, [
            'pics.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $pics = $request->file('pics');
        $arr  = [];

        foreach ($pics as $pic) {
            $input['imagename'] = uniqid(time()) . '.' . $pic->getClientOriginalExtension();
            $destinationPath    = public_path(ImgPath::TOURS);

            $pic->move($destinationPath, $input['imagename']);
            array_push($arr, $input['imagename']);
        }

        return json_encode($arr);
    }
}
