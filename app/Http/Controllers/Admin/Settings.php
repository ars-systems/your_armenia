<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings as SettingsModel;

class Settings extends Controller
{
    public function getSettings()
    {
        $data = SettingsModel::first();

        if(is_null($data)){
            $data = new \stdClass();

            $data->address = '';
            $data->id      = 0;
        }

        return view('admin.settings.index', ['data' => $data]);
    }

    public function saveAddress(Request $request)
    {
        if ($request->id == 0) {
            SettingsModel::insert(['address' => $request->address]);
        } else {
            $address = SettingsModel::find($request->id);
            $address->address = $request->address;
            $address->save();
        }

        return redirect()->back();
    }
}
