<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Enum\ImgPath;
use App\HomeImage;
use Illuminate\Http\Request;

class HomeImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $homeimages = HomeImage::where('Name', 'LIKE', "%$keyword%")
                ->orWhere('Image', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $homeimages = HomeImage::paginate($perPage);
        }

        return view('admin.home-images.index', compact('homeimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.home-images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['Image'] = $this->fileUpload($request);
        
        HomeImage::create($requestData);

        return redirect('admin/home-images')->with('flash_message', 'Home Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homeimage = HomeImage::findOrFail($id);

        return view('admin.home-images.show', compact('homeimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homeimage = HomeImage::findOrFail($id);

        return view('admin.home-images.edit', compact('homeimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $homeimage = HomeImage::findOrFail($id);
        $requestData['Image'] = $this->fileUpload($request);
        $homeimage->update($requestData);

        return redirect('admin/home-images')->with('flash_message', 'Home Image updated!');
    }

    public function fileUpload(Request $request)
    {
        $this->validate($request, [
            'Image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        
        $mainpic         = $request->file('Image');
        $imagename       = uniqid(time()) . '.' . $mainpic->getClientOriginalExtension();
        $destinationPath = public_path(ImgPath::SLIDER);
        
        $mainpic->move($destinationPath, $imagename);

        return $imagename;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $item = HomeImage::find($id);

        $mainPath = public_path('images/Slider/' . $item->Image);
        if (file_exists($mainPath)) {
            unlink($mainPath);
        }

        HomeImage::destroy($id);

        return redirect('admin/home-images')->with('flash_message', 'Home Image deleted!');
    }
}
