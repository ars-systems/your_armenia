<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;

class About extends Controller
{
	public function home()
    {
    	$pageTitle = 'About Us';
    	$address   = Settings::getAddress();

    	return \View::make('about', compact('pageTitle', 'address'));
    }
}
