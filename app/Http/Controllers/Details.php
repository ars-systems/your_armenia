<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Tour, Settings};

class Details extends Controller
{
    public function home($id)
    {
        $address   = Settings::getAddress();
    	$item      = Tour::getTour($id);
    	$tourType  = $item->type_name;
    	$pageTitle = $item->name;
    	$og        = [
            'title' => $item->name,
            'url'   => \URL('details', $item->id),
            'image' => \URL('images/Tours', $item->mainpic)
        ];

    	return \View::make('details', compact('item', 'tourType', 'pageTitle', 'og', 'address'));
    }
}
