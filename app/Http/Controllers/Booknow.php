<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Tour, Booking};

class Booknow extends Controller
{    
    private function sendmail($name, $phone, $email, $tour_id)
    {
        $tour = Tour::select(['name', 'starttime'])->whereId($tour_id)->first();

        $subject   = "New Booking for {$tour->name} on {$tour->starttime}";
        $to        = env('MAIL_TO', '');
        $message   = "Name: $name ". "Phone: $phone ". "Email: $email .";
        $emailBody = "You have received a new message from the user {$name}.". "Here is the user data: {$message} .";

        if (strlen($to) > 0) {
            \Mail::send('emails.booking', compact('emailBody'), function($message) use ($to, $subject) {
                $message->to($to)->subject($subject);
            }); 
        }
    }

    public function bookit(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'phone'   => 'required',
            'email'   => 'required|email',
            'tour_id' => 'required' 
        ]); 

        $postData = $request->all();
        $booking  = Booking::create($postData);

        $this->sendmail($postData['name'], $postData['phone'], $postData['email'], $postData['tour_id']); 

        return ['success' => true];
    }
}