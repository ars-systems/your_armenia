<?php 
namespace App;
 trait HasCats 
 { /** * A user may have multiple cats. 
 * * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany */ 
 	public function categories() 
 	{ 
 		return $this->belongsToMany(Category::class); 
 	} 
 	/** * Assign the given category to the user. 
 	* * @param string $category * * 
 	@return mixed */ 
 	public function assignCategory($category) 
 	{ 
 		return $this->categories()->save( Category::whereId($category)->firstOrFail()); 
 	} 
 	/** * Determine if the user has the given category. * * 
 	@param mixed $category * * 
 	@return boolean */ 
 	public function hasCategory($category) 
 	{ 
 		if (is_string($category)) 
 			{ 
 				return $this->categories->contains('name', $category); 
 			} 
 			if (is_array($category)) 
 				{ 
 					foreach ($category as $r) 
 						{ 
 							if ($this->hasCategory($r)) 
 								{ 
 									return true; 
 								} 
 						} 
 						return false; 
 				} 
 				return !!$category->intersect($this->categories)->count(); 
 	} 
 }