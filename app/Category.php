<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = 'cats';
    protected $fillable = ['name', 'label', 'type_id'];

    /**
     * A role may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public static function getByTour($type)
    {
        return self::select('cats.*')
            ->leftJoin('types', 'types.id', '=', 'cats.type_id')
            ->where('types.name', $type)
            ->get();
    }
}
