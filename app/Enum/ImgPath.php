<?php

namespace App\Enum;

class ImgPath
{
    const TOURS = 'images/Tours/';
    const SLIDER = 'images/Slider/';
}