<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use HasCats;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = 'tours';
    protected $fillable = 
    [
        'name',
        'description',
        'days',
        'nights',
        'starttime',
        'endtime',
        'mainpic',
        'pics',
        'type_id',
        'place',
        'days_descriptions',
        'is_best',
    ];

    public static function getTours($type, $catId = null)
    {
        if (!is_null($catId) && strtolower(Category::find($catId)->name) !== 'all') {
            $tours = self::select('tours.*')
                ->leftJoin('category_tour', 'category_tour.tour_id', '=', 'tours.id')
                ->leftJoin('types', 'types.id', '=', 'tours.type_id')
                ->where([
                    [
                        'category_tour.category_id',
                        $catId
                    ],
                    [
                        'types.name',
                        $type
                    ]
                ])
                ->paginate(12); 
        } else {
            $tours = self::select('tours.*')
                ->leftJoin('types', 'types.id', '=', 'tours.type_id')
                ->where('types.name', $type)
                ->paginate(12);
        }

        return $tours;
    }

    public static function getTour($id)
    {
        return self::select('tours.*', 'types.label as type_name', 'cats.label as tour_category')
            ->leftJoin('category_tour', 'category_tour.tour_id', '=', 'tours.id')
            ->leftJoin('cats', 'cats.id', '=', 'category_tour.category_id')
            ->leftJoin('types', 'types.id', '=', 'tours.type_id')
            ->where('tours.id', $id)
            ->first();
    }
}
