<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public $timestamps = false;
    protected $table = 'settings';

    public static function getAddress()
    {
        $data = self::first();

        if (is_null($data)) {
            $data = new \stdClass();
            $data->address = '';
        }

        return $data;
    }
}
