<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminUser
{
    const ADMINISTRATOR = 'administrator';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if (is_null($user) || !$user->hasRole(self::ADMINISTRATOR)) {
            abort(404);
        }

        return $next($request);
    }
}
