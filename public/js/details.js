$(document).ready(function(){
  const galleryThumbs = new Swiper('.gallery-thumbs', {initialSlide: 1, spaceBetween: 10,centeredSlides: true, slidesPerView: 'auto',touchRatio: 0.2,slideToClickedSlide: true, navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },});
  $(document).off('click','.show-full-screen').on('click','.show-full-screen',function(){
    var curr_img = this.getAttribute('data-img');
    $("#navB").addClass('d-none');
    $('.details-share-btns').css('opacity', 0);
    $('.img-full-screen').attr('src',curr_img);
    $('.caption').text($('.tour-title').text());
    $('.modal-backdrop').css('z-index', 0);
    $('.footer-social').addClass('d-none');
    setArrowsPositions(); 
  });
  $('#sliderModal').on('hidden.bs.modal', function(){$('.footer-social').removeClass('d-none');document.getElementById("swip").style.opacity=1;$("#navB").removeClass('d-none');$('.details-share-btns').removeAttr('style');});
  $('.slider-btn').on('click',function(){
    var curr = $('.img-full-screen').attr('src');
    var index = $(`.show-full-screen[data-img="${curr}"]`).index();
    var imgs = $('.show-full-screen');
    $('.img-full-screen').attr('src', imgs.eq($(this).hasClass('l') ? (index-1) : (index == imgs.length-1 ? 0 : (index+1))).attr('data-img'));
  });
  $(document).off('submit', '#formBookNow').on('submit', '#formBookNow', function(e){
    e.preventDefault();
    $('.buttonSend').prop('disabled', true);
    var data = {
      _token: $('[name="_token"]').val(),
      tour_id: $('.tour-id').val(),
      name: $('#bookNowName').val(),
      phone: $('#bookNowPhone').val(),
      email: $('#bookNowEmail').val()
    };
    $.post('/bookit', data).done((resp)=>{
      $('#bookNowDesc, #bookNowName, #bookNowPhone, #bookNowEmail').val('');
      $('.buttonSend').prop('disabled', false);
    });
  });
  $(window).resize(function(){
    setArrowsPositions();
  });
  function setArrowsPositions(){
    var img = $('.img-full-screen');
    var img_width = img.width()/2;
    var window_width = $('body').width()/2;
    var icon = $('.slider-btn.l');
    var left_right = window_width-img_width+(icon.width()/2);
    $('.slider-btn.l, .slider-btn.r').css('top', (img.height()/2)-(icon.height()/2));
    $('.slider-btn.l').css('left', left_right);
    $('.slider-btn.r').css('right', left_right);
  }
});