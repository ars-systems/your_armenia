$(document).ready(function ($) {
    var jssor_1_options = {
      $AutoPlay: 1,
      $SlideWidth: 720,
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$
      }
    };
    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
    var MAX_WIDTH = 980;
    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;
        if(containerWidth){
          var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
          jssor_1_slider.$ScaleWidth(expectedWidth);
        }else {
          window.setTimeout(ScaleSlider, 30);
        }
    }
    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    $(document).off('click', '.btn-more').on('click', '.btn-more', function(){
    var that = $(this);
    var page = that.attr('data-next-page');
    that.prop('disabled', true);
    $.get('best_offers', {page:page}, function(data){
      if(data.current_page > data.last_page){
        that.attr('data-next-page', data.current_page+1).prop("disabled", false);
      }else{
        that.remove();
      }
      var str = '';
      for(var i =0; i < data.data.length; i++){
        str+=`<div class="col-sm-6">
        <div class="grid">
        <figure class="effect-sadie w-100">
          <img src="images/Tours/${data.data[i].mainpic}" alt="">
          <figcaption>
          <a href="details/${data.data[i].id}"></a>
          <h2>${data.data[i].name}</h2>
          <p>${data.data[i].description.length > 50 ? (data.data[i].description.slice(0, 50)+ '...') : data.data[i].description}</p>
          </figcaption>  
        </figure>
        </div>
        </div>`;
      }
      if(str.length > 0){
        $('.best-offers-content').append(str);
      }
    });
  });
  $(document).off('submit', '#sendAnswer').on('submit', '#sendAnswer', function(e) {
    e.preventDefault();
    $('.buttonSend').prop('disabled', true);
    var data = {
      name: $('.user-name').val(),
      email: $('.user-email').val(),
      subject: $('.email-subject').val(),
      description: $('.email-description').val(),
      _token: $('._token').val(),
    };
    $.post('email', data).done((data) => {
      $('.user-name, .user-email, .email-subject, .email-description').val('');
      $('.buttonSend').prop('disabled', false);
    });
  });
})