<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Role;
use App\Permission;

class AddRolesAndPermissions extends Migration
{
    const ADMINISTRATOR = 'administrator';
    const SIMPLE_USER   = 'simple_user';
    const TABLE_PERMISSION_ROLE = 'permission_role';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roleAdmin            = Role::firstOrCreate(['name' => self::ADMINISTRATOR, 'label' => self::ADMINISTRATOR]);
        $roleSimpleUser       = Role::firstOrCreate(['name' => self::SIMPLE_USER, 'label' => self::SIMPLE_USER]);
        $permissonAdmin       = Permission::firstOrCreate(['name' => self::ADMINISTRATOR, 'label' => self::ADMINISTRATOR]);
        $permissionSimpleUser = Permission::firstOrCreate(['name' => self::SIMPLE_USER, 'label' => self::SIMPLE_USER]);

        $permissonAdminId       = $permissonAdmin->id;
        $permissionSimpleUserId = $permissionSimpleUser->id;
        $roleAdminId            = $roleAdmin->id;
        $roleSimpleUserId       = $roleSimpleUser->id;

        $checkPermissionRoleAdmin = \DB::table(self::TABLE_PERMISSION_ROLE)->where([
                'permission_id' => $permissonAdminId,
                'role_id'       => $roleAdminId
            ])->first();

        $checkPermissionRoleSimpleUser = \DB::table(self::TABLE_PERMISSION_ROLE)->where([
                'permission_id' => $permissionSimpleUserId,
                'role_id'       => $roleSimpleUserId
            ])->first();

        if (is_null($checkPermissionRoleAdmin)) {
            \DB::table(self::TABLE_PERMISSION_ROLE)->insert([
                    'permission_id' => $permissonAdminId,
                    'role_id'       => $roleAdminId
                ]);
        }

        if (is_null($checkPermissionRoleSimpleUser)) {
            \DB::table(self::TABLE_PERMISSION_ROLE)->insert([
                    'permission_id' => $permissionSimpleUserId,
                    'role_id'       => $roleSimpleUserId
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
