<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('days')->nullable();
            $table->integer('nights')->nullable();
            $table->float('price');
            $table->date('starttime');
            $table->date('endtime');
            $table->string('mainpic');
            $table->json('pics')->nullable();
            $table->integer('type_id')->unsigned();  
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
