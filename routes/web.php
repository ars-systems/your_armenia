<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'Home@home');
Route::get('home', 'Home@home');
Route::get('about', 'About@home');
Route::get('apartments', 'Apartments@home');
Route::get('accommodation', 'Accommodation@home');
Route::get('best_offers', 'Home@bestOffers');
Route::get('details/{id}', 'Details@home')->where('id', '[0-9]+');

Route::get('events', 'Tours@home');
Route::get('tours', 'Tours@home');
Route::get('packages', 'Tours@home');
Route::get('breaks', 'Tours@home');
Route::get('events/{id}', 'Tours@home')->where('id', '[0-9]+');
Route::get('tours/{id}', 'Tours@home')->where('id', '[0-9]+');
Route::get('breaks/{id}', 'Tours@home')->where('id', '[0-9]+');
Route::get('packages/{id}', 'Tours@home')->where('id', '[0-9]+');


Route::post('bookit', 'Booknow@bookit');
Route::post('email', 'Home@email');

Auth::routes();

//admin
Route::get('admin', 'Admin\AdminController@index');
Route::resource('admin/categories', 'Admin\CategoriesController');
Route::resource('admin/types', 'Admin\TypesController');
Route::resource('admin/tours', 'Admin\ToursController');
Route::resource('admin/home-images', 'Admin\HomeImagesController');
Route::get('admin/settings', 'Admin\Settings@getSettings');
Route::post('admin/settings/address', 'Admin\Settings@saveAddress');